#!python
import glob
import os
import sys
import steamclient

import requests

# Gitlab repo info
GITLAB_APNG_DIR = "Games"
GITLAB_USERNAME = "avalonparton"
GITLAB_REPO_NAME = "grid-beautification"
GITLAB_ENDPOINT = f"https://gitlab.com/api/v4/projects/{GITLAB_USERNAME}%2F{GITLAB_REPO_NAME}"

available_artwork = None

def load_artwork():
    """ Artwork is stored as nested dictionaries
    {'Dota 2': {'hero': [], 'grid': [], 'logo': []}}
    where each list contains URLs to images
    """
    global available_artwork
    if available_artwork is None:
        available_artwork = dict()
        url = f"https://gitlab.com/avalonparton/grid-beautification/raw/master/Games/APNGs.md"
        data = requests.get(url).text
        files = []
        for line in data.split('img src="')[1:]:
            path = line.split('"')[0]
            game = path.split("/")[0]
            basename = os.path.basename(path)
            url = f"https://gitlab.com/avalonparton/grid-beautification/raw/master/Games/{path}"
            
            if available_artwork.get(game) is None:
                available_artwork[game] = dict(hero=[], logo=[], grid=[])

            print("basename:", basename)
            if basename == 'logo.png':
                print("found logo:", basename)
                available_artwork[game]['logo'].append(url)
            elif basename == 'header.png':
                available_artwork[game]['hero'].append(url)
            else:
                available_artwork[game]['grid'].append(url)


load_artwork()


def get_available_artwork(game):
    """ Returns a list of available artwork from the Gitlab repo """
    default = {'logo': [], 'hero': [], 'grid': []}
    return available_artwork.get(game.name, default)


def set_artwork(game, user_id, url, kind="grid"):
    """
    Copies an image to Steam/userdata/00000/config/grid

    Args:
        game:    Any object or dict with an 'id' attribute
        user_id: User ID to set this artwork for
        url:     URL of the artwork
        kind:    Type of art (grid, hero, or logo)

    Returns the path to the new file
    """
    suffix = 'p' if kind == 'grid' else f'_{kind}'
    target = f"{STEAM_PATH}\\userdata\\{user_id}\\config\\grid\\{game.id}{suffix}.png"
    
    img = requests.get(url).content
    with open(target, 'wb') as f:
        f.write(img)
    print(f"  ✔ Copied {target}")
    return target







def get_artwork(game, user_id):
    """ Finds and applies artwork for the given game """
    name = re.sub(r'[^\x00-\x7f]|/|\\', '', game.name).strip()
    path = f"{APNG_PATH}\\{name}"
    print(f"Searching for {game.name} artwork in '{path}'")

    if os.path.isdir(path):
        # Animated cover
        apngs = [f for f in glob.glob(path + "\\*.png")
                if not os.path.basename(f) == 'logo.png'
                and not os.path.basename(f) == 'header.png']
        if len(apngs) > 0:
            target = set_artwork(game, user_id, apngs[0], kind='grid')

        # Logo and header
        for kind in ['logo', 'header']:
            image = f'{path}\\{kind}.png'
            if os.path.isfile(image):
                target = set_artwork(game, user_id, image, kind=kind)
    else:
        #pass
        print(f"Missing artwork for {game.name} ({game.id}). Request it!")
