# Artwork Manager

GUI written in Python + Javascript using Eel for managing custom artwork in the [new Steam library](https://store.steampowered.com/libraryupdate).

![screenshot](https://i.imgur.com/3DYoJGT.png)

## Running

Download and run [Artwork Manager](dist/Artwork Manager.exe) (md5 8143fab4ec3459c8f758d6f0a7f0ef66)

## Building the exe

If you want to build the .exe yourself

```bash
pip install https://github.com/pyinstaller/pyinstaller/archive/develop.tar.gz # pyinstaller for python 3.8
pip install -r requirements.txt
python -m eel .\app.py web --noconsole --onefile --icon .\assets\icon.ico --name "Artwork Manager"
```

## Running with Python

You can also run the app directly with [Python 3.8](https://www.python.org/downloads/).

```bash
pip install -r requirements.txt
python ./app.py
```

# Why?

Adding custom game covers, logos, and headers is tedious to do by hand.

We can automate this by copying the images into `C:/.../Steam/userdata/[user id]/config/grid/`

![before_and_after](https://i.imgur.com/tyo90Bd.png)

