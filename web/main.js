function foobar() {
    //var data = document.getElementById("data").value
    eel.dummy("this is eel")(function(ret) {console.log(ret)})
}

// getElementById
function $id(id) {
	return document.getElementById(id);
}

//
// output information
function Output(msg) {
	var m = $id("messages");
	m.innerHTML = msg + m.innerHTML;
}

function open_art_folder() {
    //var data = document.getElementById("data").value
    eel.open_art_folder();
}

function get_data() {
    // Calls two python functions to load the games and shortcuts list
    eel.get_games()(function(ret) {
        console.log(ret);
        document.getElementById('game-list').innerHTML = ret;
    })
    eel.get_shortcuts()(function(ret) {
        console.log(ret);
        document.getElementById('shortcut-list').innerHTML = ret;
    })
}


function install() {
    console.log("Installing artwork")
    eel.install()(function(ret) {
        console.log("Done Installing");
        document.getElementById('install-log').innerHTML = ret;
    })
}

function set_active(index, kind){
    eel.set_active(index, kind)(function(ret) {
        window.active_game = index;
        window.active_kind = kind;
        title = ret[0];
        logo = ret[1];
        hero = ret[2];
        grid = ret[3];
        available_logos = ret[4]['logo'];
        available_heros = ret[4]['hero'];
        available_grids = ret[4]['grid'];

        document.getElementById('name').innerHTML = title;
        document.getElementById('hero').src = hero;
        document.getElementById('logo').src = logo;
        document.getElementById('grid').src = grid;

        // Available logos
        element = document.getElementById('logos');
        element.innerHTML = "";
        for (var i = 0; i < available_logos.length; i++){
            url = available_logos[i]
            func = "set_artwork('"+url+"', kind='logo')"
            html = '<img src="'+url+'" width="350px" class="logo" onclick="'+func+'">\n';
            console.log(html);
            element.innerHTML += html;
        }

        // Available heros
        element = document.getElementById('heros');
        element.innerHTML = "";
        for (var i = 0; i < available_heros.length; i++){
            url = available_heros[i]
            func = "set_artwork('"+url+"', kind='hero')"
            html = '<img src="'+url+'" class="hero" onclick="'+func+'">\n';
            console.log(html);
            element.innerHTML += html;
        }

        // Available grids
        element = document.getElementById('grids');
        element.innerHTML = "";
        for (var i = 0; i < available_grids.length; i++){
            url = available_grids[i]
            func = "set_artwork('"+url+"', kind='grid')"
            html = '<img src="'+url+'" class="grid" onclick="'+func+'">\n';
            console.log(html);
            element.innerHTML += html;
        }
    })
}

function set_artwork(url, kind){
    eel.set_artwork(url, kind)(function(ret){
        // trigger animation for downloading
        set_active(window.active_game, window.active_kind);
    })
}



// Drag and drop code from 
// https://www.sitepoint.com/html5-file-drag-and-drop/
/*

// call initialization file
if (window.File && window.FileList && window.FileReader) {
	Init();
}

//
// initialize
function Init() {

	var fileselect = $id("fileselect"),
		filedrag = $id("filedrag"),
		submitbutton = $id("submitbutton");

	// file select
	fileselect.addEventListener("change", FileSelectHandler, false);

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
	
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
		filedrag.style.display = "block";
		
		// remove submit button
		submitbutton.style.display = "none";
	}

}


// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.target.className = (e.type == "dragover" ? "hover" : "");
}
// file selection
function FileSelectHandler(e) {

	// cancel event and hover styling
	FileDragHover(e);

	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;

	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		ParseFile(f);
	}

}


function ParseFile(file) {

	Output(
		"<p>File information: <strong>" + file.name +
		"</strong> type: <strong>" + file.type +
		"</strong> size: <strong>" + file.size +
		"</strong> bytes</p>"
	);
	
}
*/