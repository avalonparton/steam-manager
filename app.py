import base64
import os

import apng
import eel
from yattag import Doc

import steamclient
from artwork import get_available_artwork


def image_to_data_url(filename):
    if filename and os.path.isfile(filename):
        ext = filename.split('.')[-1]
        prefix = f'data:image/{ext};base64,'
        with open(filename, 'rb') as f:
            img = f.read()
        return prefix + base64.b64encode(img).decode('utf-8')
    else:
        return ""

eel.init('web')

# Load Steam data
users = steamclient.get_users()
active_user = users[0]
games = None
shortcuts = None
active_game = None


def format_games(games, kind="game"):
    """ Returns an HTML string for the games list """
    doc, tag, text, line = Doc().ttl()
    with tag('table'):
        with tag('tr'):
            with tag('th'):
                text("Name")
            #with tag('th'):
            #    text("App ID")
            with tag('th'):
                text("Banner")
            with tag('th'):
                text("Logo")
            with tag('th'):
                text("Cover")

        for n, game in enumerate(games):
            entry = f"{n+1} {game.name}"
            with tag('tr', onclick=f"set_active({n}, '{kind}')"):
                # Name and ID
                with tag('th'):
                    text(game.name)
                #with tag('th'):
                #    text(game.id)
                # Artwork status
                grid = game.grid
                hero = game.hero
                logo = game.logo

                for key, val in {hero:'hero', logo:'logo', grid:'grid'}.items():
                    if os.path.isfile(key): # custom art installed
                        asset = "checked.png"
                    elif len(get_available_artwork(game).get(val)) == 0: # not available
                        asset = "close.png"
                    else: # ready to download
                        asset = "download.png"
                    # generate HTML
                    with tag('th', Class="center"):
                        doc.stag('img', src=f'assets/{asset}', Class="icon", width="25px")


    return doc.getvalue()

@eel.expose
def set_active(index, kind="game"):
    """ Triggered when a user clicks on a game in the list """
    global active_game
    if kind == "game":
        game = games[index]
    else:
        game = shortcuts[index]
    if active_game is not game:
        active_game = game

    name = game.name
    logo = image_to_data_url(game.logo)
    hero = image_to_data_url(game.hero)
    grid = image_to_data_url(game.grid)

    return name, logo, hero, grid, get_available_artwork(game)

@eel.expose
def set_artwork(url, kind):
    if kind == "logo":
        active_game.set_logo(url=url)
    elif kind == "grid":
        active_game.set_grid(url=url)
    elif kind == "hero":
        active_game.set_hero(url=url)

@eel.expose
def get_games():
    global games
    print("Searching for installed games")
    games = active_user.games()
    print(f"Found {len(games)} games!")
    return format_games(games)
    
@eel.expose
def get_shortcuts():
    global shortcuts
    print("Searching for non-steam shortcuts")
    shortcuts = active_user.shortcuts
    print(f"Found {len(shortcuts)} shortcuts!")
    return format_games(shortcuts, kind="shortcut")

@eel.expose
def open_art_folder():
    if games is None:
        get_games()
    game = games[0]
    folder = os.path.dirname(game.grid)
    os.startfile(folder)
    

#eel.start('index.html', mode="chrome-app", port=9169)
eel.start('index.html', mode="chrome", port=9169, size=(1200,1200))
